local peripheralList = peripheral.getNames()
for i = 1, #peripheralList do
    local periItem = peripheralList[i]
    local periType = peripheral.getType(periItem)
    local peri = peripheral.wrap(periItem)

    print("Peri Type: "..periType)

    if(periType == "periType") then
        for key,value in pairs(peri) do
            print("found method " .. key);
        end
    end
end