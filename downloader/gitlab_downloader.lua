-- Test Script Downloader (GitLab) --

--===== Local variables =====

--Branch & Relative paths to the url and path
local relPath = "/testscripts/"
local repoUrl = "https://gitlab.com/seekerscomputercraft/testscripts/-/raw/"
local branch = "main"
local relUrl = repoUrl..branch.."/"

--Select the github branch to download
function selectBranch()
	clearTerm()

	print("Which version should be downloaded?")
	print( "Available:")
	print("1) main")
	print("2) develop")
	print("Input (1-2):")

	local input = read()
	if input == "1" then
		branch = "main"
		relUrl = repoUrl..branch.."/"
	elseif input == "2" then
		branch = "develop"
		relUrl = repoUrl..branch.."/"
	else
		print("Invalid input!")
		sleep(2)
		selectBranch()
	end
end

--Removes old installations
function removeAll()
	if fs.exists(relPath) then
		shell.run("rm "..relPath)
	end
	if fs.exists("startup") then
		shell.run("rm startup")
	end
end

--Writes the files to the computer
function writeFile(path)
	local file = fs.open(relPath..path,"w")
	local content = getURL(path);
	file.write(content)
	file.close()
end

--Resolve the right url
function getURL(path)
	local gotUrl = http.get(relUrl..path)
	if gotUrl == nil then
		clearTerm()
		error("File not found! Please check!\nFailed at "..relUrl..path)
	else
		return gotUrl.readAll()
	end
end

--Clears the terminal
function clearTerm()
	shell.run("clear")
	term.setCursorPos(1,1)
end

function downloadAndRead(fileName)
	writeFile(fileName)
	local fileData = fs.open(relPath..fileName,"r")
	local list = fileData.readAll()
	fileData.close()

	return textutils.unserialise(list)
end

function getAllFiles()
	local fileEntries = downloadAndRead("files.txt")

	for k, v in pairs(fileEntries) do
	  print(v.name.." files...")

	  for fileCount = 1, #v.files do
      local fileName = v.files[fileCount]
      writeFile(fileName)
	  end

	  print("Done...")
	end
end
selectBranch()
removeAll()
getAllFiles()